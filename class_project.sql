-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2021 at 11:41 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `class_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `corder` int(11) NOT NULL,
  `cstatus` enum('active','inactive') NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdate` date NOT NULL DEFAULT current_timestamp(),
  `updatedate` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`, `corder`, `cstatus`, `image`, `createdate`, `updatedate`) VALUES
(2, 'sd card', 6408633, 'active', '1612356054-4645-nasa.png', '2021-02-03', '2021-02-03');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `SalePrice` int(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `quantity` int(11) NOT NULL,
  `currentdate` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `category`, `Price`, `SalePrice`, `status`, `quantity`, `currentdate`, `updatedate`) VALUES
(9, 'dfg', 2, 5, 5, 'inactive', 5, '2021-02-04 11:54:28', '2021-02-04 11:54:28'),
(10, 'asdff', 2, 25, 5, 'active', 5, '2021-02-04 12:00:33', '2021-02-04 12:00:33');

-- --------------------------------------------------------

--
-- Table structure for table `p_images`
--

CREATE TABLE `p_images` (
  `i_id` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `istatus` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p_images`
--

INSERT INTO `p_images` (`i_id`, `productid`, `img`, `istatus`) VALUES
(1, 9, '3562-android.png', 'inactive'),
(2, 9, '3562-bat.jpg', 'active'),
(3, 9, '3562-blub.jpg', 'inactive'),
(4, 9, '3562-cat.jpeg', 'inactive'),
(5, 9, '3562-fb.png', 'inactive'),
(6, 9, '3562-groot.jpg', 'inactive'),
(7, 9, '3562-joker.jpg', 'inactive'),
(8, 9, '3562-mobile.jpg', 'inactive'),
(9, 9, '3562-nasa.png', 'inactive'),
(10, 9, '3562-nature.jpg', 'inactive'),
(11, 9, '3562-smile.jpeg', 'inactive'),
(12, 9, '3562-super.jpg', 'inactive'),
(13, 9, '3562-truck.jpg', 'inactive'),
(14, 9, '3562-vsghdv.jfif', 'inactive'),
(15, 10, '5778-alien.jpeg', 'inactive'),
(16, 10, '5778-android.png', 'inactive'),
(17, 10, '5778-bat.jpg', 'inactive'),
(18, 10, '5778-blub.jpg', 'inactive'),
(19, 10, '5778-cat.jpeg', 'inactive'),
(20, 10, '5778-fb.png', 'inactive'),
(21, 10, '5778-groot.jpg', 'active'),
(22, 10, '5778-joker.jpg', 'inactive'),
(23, 10, '5778-mobile.jpg', 'inactive'),
(24, 10, '5778-nasa.png', 'inactive'),
(25, 10, '5778-nature.jpg', 'inactive'),
(26, 10, '5778-smile.jpeg', 'inactive'),
(27, 10, '5778-super.jpg', 'inactive'),
(28, 10, '5778-truck.jpg', 'inactive'),
(29, 10, '5778-vsghdv.jfif', 'inactive');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `password`) VALUES
(1, 'tpr12@gmail.com', 'tarang', 123);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cat_name` (`cat_name`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `p_images`
--
ALTER TABLE `p_images`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `p_images`
--
ALTER TABLE `p_images`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
