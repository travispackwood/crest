<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('addcategory');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cname' => 'required',
            'order' => 'required',
            'status' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $imagename = '';
        if ($request->image) {
            $imagename = time() . '.' . $request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $imagename);
        }
        $data = new Category; //model name
        $data->cname = $request->cname; //from name
        $data->order = $request->order;
        $data->status = $request->status;
        $data->image = $imagename;
        $data->save();
        return redirect('view-category')->with('success', 'successfully.');
        // ->route('category')
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $cat = Category::all();
        return view('view-category')->with('cat1', $cat);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fedit = Category::find($id);
        return view('edit')->with('edit', $fedit );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if ($request->image) {
            $imagename = '';
            if ($request->image) {
                $imagename = time() . '.' . $request->image->getClientOriginalName();
                $request->image->move(public_path('uploads'), $imagename);
            }
        } else {
            $imagename = $request->oldimage;
        }
        $edit = Category::find($request->id);
        $edit->cname = $request->cname; //from name
        $edit->order = $request->order;
        $edit->status = $request->status;
        $edit->image = $imagename;
        $edit->update();
        return redirect('view-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $delete = Category::find($id)->delete();
            return redirect('view-category')->with('delete', $delete);
    }
}
