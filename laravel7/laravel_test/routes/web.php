<?php

use App\Category;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('login','login');
Route::post('login','UserController@index');
Route::view('demol','demol');

Route::get('logout/', function () {
    session()->forget('data');
    return redirect('login');
});

Route::get('/admin', function () {
    return view('admin.master');
});
Route::post('/store','CategoryController@store')->name('store');//insert data store
Route::get('/category','CategoryController@index')->name('category');//

Route::get('/view-category','CategoryController@show');//display page

Route::post('/edit/{id}','CategoryController@update')->name('Edit');
Route::get('/update/{id}','CategoryController@edit')->name('update');

// Route::post('/edit/{id}', 'CategoryController@edit')->name('edit');
// Route::get('/update/{id}', 'CategoryController@update')->name('upadte');


Route::get('/delete/{id}','CategoryController@destroy')->name('delete');//delete page


// Route::resource('category', 'CategoryController');
