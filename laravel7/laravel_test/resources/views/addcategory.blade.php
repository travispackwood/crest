@extends('admin.master')

@section('content')
<!DOCTYPE html>

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->


</head>

<body>


    <div class="container">
        <center>
            <h2>AddCategory</h2>
        </center>
        <form action="{{route('store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
            <div class="form-group  col-xs-10" >
                <label>Name:</label>
                <input type="text" name="cname" class="form-control" id="name" >
            </div>
            <br>
            <div class="form-group col-xs-10">
                <label>order:</label>
                <input type="number" name="order" class="form-control" id="number" value="<?php echo (mt_rand(10000, 9000000)); ?>">
            </div>
            <br>

            <div class="form-group col-xs-10">
                <label>Status</label>
                <select class="form-control" name="status">
                    <option value="">----Select----</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
            <br>

            <div class="form-group col-xs-10">
                <label class="form-label" for="customFile">Image</label>
                <br>
                <input type="file" name="image" id="customFile" />
                <br><br>

                <button href type="submit" class="btn btn-success" name="insert-btn">Submit</button>

        </form>
    </div>
</body>

</html>
@endsection