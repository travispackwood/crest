@extends('admin.master')
@section('content')

<h2>viewCategory</h2><br>

<form action="" method="POST">
    <input class="col-md-4" type="text" placeholder="Search.." name="search">
    <button type="submit" name="submit" class="btn btn-danger" style= "font-size:12px;"><i class="fa fa-search"></i></button><br><br>

    <a type="button" class="btn btn-warning" href="{{route('category')}}">Add-category</a><br><br>


</form>


<table class=" table table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>order</th>
            <th>status</th>
            <th>image</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>

    <tbody>

    @foreach($cat1 as $val)
        <tr>
            <td>{{$val->id}}</td>
            <td>{{$val->cname}}</td>
            <td>{{$val->order}}</td>
            <td>{{$val->status}}</td>
            <td>
                <img src="{{ asset('public/uploads/'.$val->image ) }}" width="50px" height="50px">
            </td>
            <td><a href="{{route('update',[$val->id])}}" class="btn btn-primary">Edit</a></td>
            <td><a href="{{route('delete',[$val->id])}}" class="btn btn-danger">DELETE</a><td>
        </tr>
        @endforeach

    </tbody>

</table>
@endsection