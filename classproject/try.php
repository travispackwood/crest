<?php
// include("Navigationbar.php");

include("helper.php");


if (isset($_POST['insert-btn'])) {

  $category_name = $_POST['category_name'];
  $category_order = $_POST['category_order'];
  $category_status = $_POST['category_status'];
  $category_image = $_FILES['c_image'];
  $image = $category_image['name'];
 $tmp_name = $category_image['tmp_name'];

 $path='images/';
  $filerror = $category_image['error'];

  if ($filerror == 0) {

    $new_name=$db->image_upload($image, $tmp_name, $path);


$a = [
    "cat_name" => $category_name,
    "corder" => $category_order,
    "cstatus" => $category_status,
    "image" => $new_name
];
// print_r($a);
// exit();

$sql = $db->insert('category',$a) or die(mysqli_error($db->conn));

    if($sql) {
      header('location:view-Category.php');
    } else {
      echo "not done";
    }
  }
}

?>




<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Show Bootstrap Modal Window Using jQuery</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<meta charset="utf-8">
<title>Show Bootstrap Modal Window Using jQuery</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/jquery-3.5.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>




<script>
    $(document).ready(function(){
        $(".btn").click(function(){
            $("#myModal").modal('show');
        });
    });
</script>
<style>
    .bs-example{
        margin: 20px;
    }
</style>
</head>
<body>
<div class="bs-example">
    <!-- Button HTML (to Trigger Modal) -->
    <a type="button" class="btn btn-lg btn-primary">Show Modal</a>

    <!-- Modal HTML -->
    <div id="myModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal Title</h5>
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">
                <form action=" " method="POST" enctype="multipart/form-data">
      <div class="form-group">
        <label>Name:</label>
        <input type="text" name="category_name" class="form-control" id="name" required="">
      </div>
      <br>

      <div class="form-group">
        <label>order:</label>
        <input type="number" name="category_order" class="form-control" id="number" value="<?php echo (mt_rand(10000, 9000000)); ?>" required="">
      </div>
      <br>

      <div class="form-group">
        <label>Status</label>
        <select class="form-control" name="category_status" required="">
          <option value="">----Select----</option>
          <option value="Active">Active</option>
          <option value="Inactive">Inactive</option>
        </select>
      </div>
      <br>

      <div class="form-group">
        <label class="form-label" for="customFile">Image</label>
        <br>
        <input type="file" name="c_image" id="customFile" /><br>

        <button type="submit" class="btn btn-success" name="insert-btn">Submit</button>

    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>