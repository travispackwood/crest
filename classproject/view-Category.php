<?php
include("Navigationbar.php");
include("helper.php");

?>

<!DOCTYPE html>
<html>

<head>
</head>

<body>
    <div class="container">
        <h2>VIEW CATEGORY</h2>

        <form action="" method="POST">
            <input type="text" class=" col-md-4" placeholder="Search.." name="search">
            <button name="ssubmit" type="submit"><i class="fa fa-search"></i></button><br><br>

            <!-- <a type="button"  class="btn btn-lg btn-primary"  href="try.php">Add-category</a><br><br> -->
            <a   type="button" href="Add-Category.php" class="btn btn-primary">Add-Category</a>

        </form>
        <br>

        <div style="background-color: #E7EAF0;">
            <?php
            if (isset($_POST['ssubmit'])) {
                $sname = $_POST['search'];


                $where = "cat_name LIKE '%$sname%' OR  `corder` LIKE '%$sname%' OR  `cstatus` LIKE '%$sname%' ";
                $sql = $db->select('*', 'category', '',"WHERE $where", '', '') or  die(mysqli_error($db->conn));
            } else {

                $sql = $db->select('*', 'category', '', '', '', '');

            }


            ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>order</th>
                        <th>status</th>
                        <th>image</th>
                        <th colspan="2">Action</th>

                    </tr>
                </thead>
                <tbody id="table">
                    <?php
                    //while ($res = mysqli_fetch_array($query))
                    foreach ($sql as $key => $val) {
                    ?>

                        <tr>
                            <td><?php echo $val['id']; ?></td>
                            <td><?php echo $val['cat_name']; ?></td>
                            <td><?php echo $val['corder']; ?></td>
                            <td><a class="<?php echo $val['cstatus'] == 'active' ? " btn btn-success" : " btn btn-danger" ?>" href="status.php?id=<?php echo $val['id']; ?>" name="change"><?php echo $val['cstatus']; ?></a></td>
                            <td>
                                <img src="images/<?php echo $val['image']; ?>" height="60px" width="60px">
                            </td>
                            <td> <a href="c_update.php?id=<?php echo $val['id']; ?>" <i style="font-size:25px;color:blue" class="fa fa-edit"></i> </a></td>
                            <td><button onclick="myfun(<?php echo $val['id']; ?>)" href="c_delete.php?id=<?php echo $val['id']; ?>" <i style="font-size:25px;color:red" class="fa fa-trash"></i></button></td>
                        </tr>

                    <?php } ?>

                </tbody>
            </table>
            <?php
            //}
            ?>
        </div>
    </div>

</body>
<script>
    function myfun(delid) {

        let id = delid;

    swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "GET",
                        url: "c_delete.php",
                        data: {
                            id: id
                        },

                        success: function(value) {
                            $("#table").html(value);
                            location.reload();
                        }


                    });


                }
            });
    }
</script>


</html>