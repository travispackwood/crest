<?php
class Database
{
    public $dbHost;
    public $dbUser;
    public $dbPass;
    public $dbName;
    public $conn;
    //  public $selectdb;
    // public $cnt;
    // public $condition;

    public function __construct()
    {
        $this->dbHost = "localhost";
        $this->dbUser = "root";
        $this->dbPass = "";
        $this->dbName = "class_project";
        $this->conn = "";
        //      $this->selectdb="";
        $this->conn = mysqli_connect($this->dbHost, $this->dbUser, $this->dbPass, $this->dbName);
        if (!$this->conn) {
            die("connection fail");
        }
    }


    public function select($sel, $table, $JOINS, $where,$groupby='', $order='')
    {
        //  $where="";
        //  if($values!=""){
        //  foreach($values as $key =>$value){  $where .=$key."='".$value."'";}
        // print_r($where);
        // exit();
        //  }
        $query = "SELECT $sel from `$table`" . $JOINS . $where . $groupby . $order;
    //    echo $query;
    //     exit;
        //echo $where;
        $result = mysqli_query($this->conn, $query);
        // print_r($result);
        $return = [];
        while ($row = mysqli_fetch_array($result)) {
            $return[] = $row;

            // echo "<pre/>";
            //  print_r($return);exit;
        }
        return $return;
    }


    public function insert($table, $field)
    {
        $key = array_keys($field);
        $val = array_values($field);
        // print_r($val);
        // exit();
        $return = [];
        $query = "INSERT into  $table(`". implode('`,`', $key) . "`)" . "Values('" . implode("','", $val) . "')";
        // print_r($query);
        // exit();
        $result = mysqli_query($this->conn,$query);
        return $result;
    }


    public function update($table, $values, $where)
    {
        $set = [];
        foreach ($values as $key => $value) {
         $set[] =  "`".$key."`" . "='" . $value . "'";
        }
        $query = "UPDATE $table SET " . implode(',', $set) . " " . "WHERE" . " " . $where;
        // echo $query;
        // exit;
        $result = mysqli_query($this->conn, $query);
        return $result;
    }



    public function delete($table,$where, $values,$JOINS='')
    {
        $where = "";
        if ($values != "") {
            foreach ($values as $key => $value) {
                $where .= $key . "='" . $value . "'";
            }

        }
        $query = "DELETE FROM $table" . " " . "WHERE" . " " . $where;
        // echo $query;
        // exit;
        $result = mysqli_query($this->conn, $query);
        return $result;
    }


    public function getInsertId()
    {
        $_result = mysqli_insert_id($this->conn);
        $this->_insertId = $_result;
        return $_result;
    }


    function image_upload($image, $tmp_name, $path)
    {
        $new_name = ltrim(time() . "-" . rand(1000, 9999) . "-" . $image);
        $targetFilePath = $path . $new_name;
        $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
        $allowed = array("jpg", "jpeg", "png");
        if (in_array($fileType, $allowed)) {
            move_uploaded_file($tmp_name, $targetFilePath);
            return $new_name;
        } else {
            return false;
        }
    }


    function row1($v)
    {

        echo "<tr>";
        echo "<td>" . $v['cat_id'] . "</td>";
        echo "<td>" . $v['cat_name'] . "</td>";
        echo "<td>" . '<img src="category_images/' . $v["cat_image"] . '" height="80px" width="120px">' . "</td>";
        echo "<td>" . $v['order_id'] . "</td>";
        echo "<td>" . $v['cat_order'] . "</td>";
        echo "<td>" . $v['status'] . "</td>";
        echo "<td>" . $v['created_at'] . "</td>";
        echo "<td>" . $v['updated_at'] . "</td>";
        echo "<td>" . '<a href="delete_cat.php?delid=' . $v["cat_id"] . '"><button class="btn btn-danger">Delete</button></a>' . "</td>";
        echo "<td>" . '<a href="edit_cat.php?editid=' . $v["cat_id"] . '"><button class="btn btn-primary">Edit</button></a>' . "</td>";
        echo "</tr>";
    }

    function row2($v)
    {
        echo "<tr>";
        echo "<td>" . $v['id'] . "</td>";
        echo "<td>" . $v['name'] . "</td>";
        echo "<td>" . '<img src="product_images/' . $v["img_name"] . '" height="80px" width="120px"> ' . "</td>";
        echo "<td>" . $v['cat_name'] . "</td>";
        echo "<td>" . $v['product_code'] . "</td>";
        echo "<td>" . $v['price'] . "</td>";
        echo "<td>" . $v['sale_price'] . "</td>";
        echo "<td>" . $v['status'] . "</td>";
        echo "<td>" . $v['created_at'] . "</td>";
        echo "<td>" . $v['updated_at'] . "</td>";
        echo "<td>" . '<a href="delete_product.php?delid=' . $v["id"] . '"><button class="btn btn-danger">Delete</button></a>' . "</td>";
        echo "<td>" . '<a href="edit_product1.php?editid=' . $v["id"] . '"><button class="btn btn-primary">Edit</button></a>' . "</td>";
        echo "</tr>";
    }

    // public function  getRow($row){
    //     $row=count($row);
    //     return $row;
    // }
}

$db = new Database();
