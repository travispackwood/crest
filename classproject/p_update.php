<?php

include("helper.php");
include("Navigationbar.php");


$pid = $_GET['id'];
$where = " WHERE id = '$pid'";
$sql = $db->select('*', 'product', '',  $where, '', '') or die(mysqli_error($db->conn));

if (isset($_POST['updatedata'])) {

    $product_name = $_POST['product_name'];
    $product_category = $_POST['product_category'];
    $product_price = $_POST['product_price'];
    $product_saleprice = $_POST['product_saleprice'];
    $product_status = $_POST['product_status'];
    $product_quantity = $_POST['product_quantity'];
    $date = date('Y-m-d H:i:s');
    $product_image = $_FILES['update_image']['name'];

    if ($product_image != '') {
        $val = array(
            "name" => $_POST['product_name'],
            "category" => $_POST['product_category'],
            "Price" => $_POST['product_price'],
            "SalePrice" => $_POST['product_saleprice'],
            "status" => $_POST['product_status'],
            "quantity" => $_POST['product_quantity'],
        );

        $where3 = "id = '$pid'";
        $update = $db->update('product', $val, $where3) or die(mysqli_error($db->conn));


        for ($i = 0; $i < count($product_image); $i++) {
            $status = 'inactive';

            $filename = $_FILES['update_image']['name'][$i];
            if (move_uploaded_file($_FILES['update_image']['tmp_name'][$i], 'proimage/' . $filename)) {
                $a1 = [
                    "productid" => $pid,
                    "img" =>  $filename,
                    "istatus" => $status,
                ];
                $sql = $db->insert('p_images', $a1) or die(mysqli_error($db->conn));
            }
        }
    } else {
        $update = $db->update('product', $val, $where3) or die(mysqli_error($db->conn));
    }
    header('Location: view-Product.php');
}



?>

<!DOCTYPE html>
    <html>
    <body>
            <div class="container">
                <div class="row justify-content-center">
                <div class="col-lg-12">
                    <h2 class="text-center">UpdateProduct</h2>
                    <form action="" method="POST" enctype="multipart/form-data">
                    <?php
                    foreach ($sql as $key => $arrdata) {
                    ?>
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" name="product_name" class="form-control" placeholder="Enter Name" value="<?php echo $arrdata['name']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Category:</label>
                            <select class="form-control" name="product_category" value="<?php echo $arrdata['category']; ?>">
                                <?php
                                $where1 = " WHERE cstatus='active'";
                                $sql1 = $db->select('*', 'category', '', $where1, '', '') or die(mysqli_error($db->conn));

                                foreach ($sql1 as $key => $value1) {
                                ?>
                                    <option value="<?php echo $sql1 = $value1['id']; ?>">
                                        <?php echo $sql1 = $value1['cat_name']; ?>
                                    </option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Price:</label>
                            <input type="text" name="product_price" class="form-control" value="<?php echo $arrdata['Price']; ?>">
                        </div>
                        <div class="form-group">
                            <label>SalePrice:</label>
                            <input type="text" name="product_saleprice" class="form-control" value="<?php echo $arrdata['SalePrice']; ?>">
                        </div>
                        <div class="form-group">
                            <label>Status:</label>
                            <select class="form-control" name="product_status" value="<?php echo $arrdata['status']; ?>">
                                <option value="active">active</option>
                                <option value="inactive">inactive</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Quantity:</label>
                            <input type="text" name="product_quantity" class="form-control" value="<?php echo $arrdata['quantity']; ?>">
                        </div>
                        <div>
                            <label class="form-label" for="customFile">Image:</label>
                            <?php

                            $where2 = " WHERE productid={$pid}";
                            $sql2 = $db->select('*', 'p_images', '', $where2, '', '') or die(mysqli_error($db->conn));

                            foreach ($sql2 as $key => $value2) {
                                if ($value2['istatus'] == 'active') {
                                    $image[] = $value2['img'];
                                } elseif($value2['istatus'] == 'inactive') {
                                    $in_image[] = $value2['img'];
                                    $new_id[] = $value2['i_id'];
                                }
                            }

                            ?>

                            <input type="file" name="update_image[]" class="form-control" multiple value="<?php echo $arrdata['img']; ?>">

                            <?php for ($i = 0; $i < count($image); $i++) { ?>
                                <img src="proimage/<?php echo $image[$i]; ?>" width="90px" height="90px" style="border:5px solid green"><br><br>
                            <?php } ?>
                            <?php for ($i = 0; $i < count($in_image); $i++) { ?>
                                <img src="proimage/<?php echo $in_image[$i]; ?>" width="80px" height="80px" style="border:2px  solid red">
                                <a href='image_delete.php?pid=<?php echo $pid; ?>&img_id=<?php echo $new_id[$i]; ?>' class='btn btn-danger'>Delete</a>
                                <a href='image_update.php?pid=<?php echo $pid; ?>&img_status=<?php echo $new_id[$i]; ?>' class='btn btn-primary'>Active</a>
                                <?php }
                            ?>
                        </div>
                        <?php
                    }

                        ?>
                        <center>
                                <button class="btn btn-success" class="form-control" class="center" name="updatedata">submit</button>
                        </center>
                    </form>




                </div>
        </div>

    </div>
            </body>
    </html>
