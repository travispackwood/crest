<?php
include("Navigationbar.php");

include("helper.php");


if (isset($_POST['insert-btn'])) {

  $category_name = $_POST['category_name'];
  $category_order = $_POST['category_order'];
  $category_status = $_POST['category_status'];
  $category_image = $_FILES['c_image'];
  $image = $category_image['name'];
 $tmp_name = $category_image['tmp_name'];

 $path='images/';
  $filerror = $category_image['error'];

  if ($filerror == 0) {

    $new_name=$db->image_upload($image, $tmp_name, $path);


$a = [
    "cat_name" => $category_name,
    "corder" => $category_order,
    "cstatus" => $category_status,
    "image" => $new_name
];
// print_r($a);
// exit();

$sql = $db->insert('category',$a) or die(mysqli_error($db->conn));

    if($sql) {
      header('location:view-Category.php');
    } else {
      echo "not done";
    }
  }
}

?>

<!DOCTYPE html>

<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


</head>

<body>




  <div class="container">
    <center>
      <h2>AddCategory</h2>
    </center>
    <form action=" " method="POST" enctype="multipart/form-data">
      <div class="form-group">
        <label>Name:</label>
        <input type="text" name="category_name" class="form-control" id="name" required="">
      </div>
      <br>

      <div class="form-group">
        <label>order:</label>
        <input type="number" name="category_order" class="form-control" id="number" value="<?php echo (mt_rand(10000, 9000000)); ?>" required="">
      </div>
      <br>

      <div class="form-group">
        <label>Status</label>
        <select class="form-control" name="category_status" required="">
          <option value="">----Select----</option>
          <option value="Active">Active</option>
          <option value="Inactive">Inactive</option>
        </select>
      </div>
      <br>

      <div class="form-group">
        <label class="form-label" for="customFile">Image</label>
        <br>
        <input type="file" name="c_image" id="customFile" /><br>

        <button type="submit" class="btn btn-success" name="insert-btn">Submit</button>

    </form>
  </div>

</body>

</html>