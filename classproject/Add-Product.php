<?php

include("Navigationbar.php");
include("helper.php");


$where = " WHERE cstatus='active'";
$sql = $db->select('*', 'category', '',$where, '', '') or die(mysqli_error($db->conn));


if (isset($_POST['insertData'])) {

    $product_name = $_POST['product_name'];
    $product_category = $_POST['product_category'];
    $product_price = $_POST['product_price'];
    $product_saleprice = $_POST['product_saleprice'];
    $product_status = $_POST['product_status'];
    $product_quantity = $_POST['product_quantity'];
    $product_image = $_FILES['product_image'];
    $filename = $product_image['name'];
   // $new_name =[time().",".rand(100,999).",".  $filename];
    $new_name=(mt_rand(1000,9999));





    $filepath = $product_image['tmp_name'];
    $path = 'proimage/';


   $a = [
    "name" => $product_name,
    "category" => $product_category,
    "Price" => $product_price,
    "SalePrice" => $product_saleprice,
    "status"=>$product_status,
    "quantity"=>$product_quantity
];

$sql = $db->insert('product',$a) or die(mysqli_error($db->conn));

    $error = array();

    if (empty($_FILES['product_image']['name'][0])) {
        $error[] = "please select image";
    } else {


        foreach ($_FILES['product_image']['name'] as $val) {
            $ext = strtoupper(substr($val, -4));
            if (!($ext == ".JPG" || $ext == ".PNG" || $ext == "JPEG" || $ext == "JFIF")) {
                $error[] = "wrong image type";
        }
        }
    }
    if (!empty($error)) {
        foreach ($error as $er) {
            echo "$er";
        }
    } else {

        if ($sql == 1) {
            $lastid = mysqli_insert_id($db->conn);
        }

           foreach ($filepath as $key => $value) {
            $imagename = $filename[$key];
            $imagetemp = $filepath[$key];
            //$rendom =(mt_rand(100, 500));
            $nm= $new_name.'-'. $imagename;

            if (move_uploaded_file($imagetemp, $path .$new_name.'-'. $imagename)) {
                if ($key == 0) {
                    $status = 'active';
                } else {
                    $status = 'inactive';
                }
                $a1 = [
                    "productid" => $lastid,
                    "img" => $nm,
                    "istatus" => $status,
                ];
                $sql = $db->insert('p_images',$a1) or die(mysqli_error($db->conn));
                header('location:view-Product.php');



            }
        }
    }
}

?>



<!DOCTYPE html>
<html>

<head>
<!-- <script src="https://parsleyjs.org/dist/parsley.js"></script>
<style type="text/css">.req {margin:2px;color:#dc3545;}.serif {font-family: "Times New Roman", Times, serif;}li.parsley-required {
    color: red;
	}</style> -->

</head>

<body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <h2 class="text-center">AddProduct</h2>

                <form actinon="#" method="POST" enctype="multipart/form-data" data-parsley-validate="">
                    <div class=" form-group">
                        <label>Name:</label>
                        <input type="text" name="product_name" class="form-control" placeholder="Enter Name">
                    </div>
                    <div class="form-group">
                        <label>Category:</label>
                        <select class="form-control" name="product_category">
                            <?php
                           // while ($row = $res->fetch_object())
                            foreach ($sql as $key => $value){
                            ?>
                                <option value="<?php echo $sql = $value['id'];?>">
                                    <?php echo $sql= $value['cat_name']; ?>
                                </option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Price:</label>
                        <input type="number" name="product_price" class="form-control" required="">
                    </div>
                    <div class="form-group">
                        <label>SalePrice:</label>
                        <input type="number" name="product_saleprice" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Status:</label>
                        <select  class="form-control" name="product_status" required="">
                        <option value="">--Select--</option>
                            <option value="active">active</option>
                            <option value="inactive">inactive</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Quantity:</label>
                        <input type="number" name="product_quantity" class="form-control"  required="">
                    </div>
                    <div>
                        <label class="form-label" for="customFile">Image:</label>
                        <input type="file"  id="customFile" name="product_image[]" multiple />

                    </div>
                    <div>
                        <br>

                        <button class="btn btn-success" class="form-control" class="center" name="insertData">submit</button>

                    </div>

                </form>
            </div>
        </div>

    </div>


</body>

</html>