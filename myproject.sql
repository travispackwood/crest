-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2021 at 11:43 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdate` date NOT NULL DEFAULT current_timestamp(),
  `updatedate` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `order`, `status`, `image`, `createdate`, `updatedate`) VALUES
(84, 'aser', 6738555, 'active', '1612765437,786,android.png', '2021-02-08', '2021-02-08'),
(85, 'edf', 4375521, 'active', '1612765445,385,bat.jpg', '2021-02-08', '2021-02-08'),
(86, 'df', 3095968, 'inactive', '1612766176,516,bat.jpg', '2021-02-08', '2021-02-08'),
(87, 'dsffg', 6795032, 'active', '1613051846,598,android.png', '2021-02-11', '2021-02-11');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `Price` int(11) NOT NULL,
  `SalePrice` int(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `quantity` int(11) NOT NULL,
  `currentdate` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedate` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `category`, `Price`, `SalePrice`, `status`, `quantity`, `currentdate`, `updatedate`) VALUES
(19, 'esfwe', 84, 25, 54, 'active', 8, '2021-02-11 19:27:48', '2021-02-11 19:27:48'),
(20, 'sedfstfg', 85, 89, 54, 'active', 54, '2021-02-11 19:31:03', '2021-02-11 19:31:03'),
(21, 'dfsgfgdf', 87, 5, 69, 'active', 54, '2021-02-11 19:31:30', '2021-02-11 19:31:30');

-- --------------------------------------------------------

--
-- Table structure for table `p_images`
--

CREATE TABLE `p_images` (
  `id` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `istatus` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `p_images`
--

INSERT INTO `p_images` (`id`, `productid`, `img`, `istatus`) VALUES
(149, 19, '1006-alien.jpeg', 'active'),
(150, 19, '1006-android.png', 'inactive'),
(151, 19, '1006-bat.jpg', 'inactive'),
(152, 19, '1006-blub.jpg', 'inactive'),
(153, 19, '1006-cat.jpeg', 'inactive'),
(154, 20, '1334-groot.jpg', 'active'),
(155, 20, '1334-joker.jpg', 'inactive'),
(156, 20, '1334-mobile.jpg', 'inactive'),
(157, 20, '1334-nasa.png', 'inactive'),
(158, 21, '6330-super.jpg', 'active'),
(159, 21, '6330-truck.jpg', 'inactive'),
(160, 21, '6330-vsghdv.jfif', 'inactive');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `password`) VALUES
(1, 'tarang12@gmail.com', 'tarang', 123);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_images`
--
ALTER TABLE `p_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `p_images`
--
ALTER TABLE `p_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
